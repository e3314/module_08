terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

#настройки провайдера YandexCloud
provider "yandex" {
  token     = "AQAAAAAO0BS2AATuwYhO1Mb79U5nk8KQ5N9aYug"
  cloud_id  = "b1g1t889hm3r00ls98rs"
  folder_id = "b1gbcsac8sftjd9m78vj"
  zone      = "ru-central1-a"
}

#Создаем группу из вирутальных машин для серверов NGINX
resource "yandex_compute_instance_group" "ig-1" {

  name = "fixed-ig"
  folder_id = "b1gbcsac8sftjd9m78vj"
  service_account_id = "ajeqc71qkmkvii7ghsg1"

  instance_template {
    platform_id = "standard-v1"

    resources {
      cores = 2
      memory = 2
    }

    boot_disk {
      initialize_params {
        image_id = "fd8etnr6krbm4llmjgtn"
        size = 40
      }
    }

    network_interface {
      subnet_ids = [yandex_vpc_subnet.subnet-2.id]
      nat = true
      security_group_ids = [yandex_vpc_security_group.group2.id]
    }

    metadata = {
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
  }

  scale_policy {

    fixed_scale {
      size = 2
    }

  }

  allocation_policy {

    zones = ["ru-central1-a"]

  }

  deploy_policy {

    max_unavailable = 1
    max_expansion = 0

  }

  load_balancer {
    target_group_name        = "react-group"
    target_group_description = "load balancer target group"
  }

}

#Создаем вирутальную машину для ReactJS
resource "yandex_compute_instance" "vm-1" {

  name        = "react-vm"
  platform_id = "standard-v1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8etnr6krbm4llmjgtn"
      size = 40
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-2.id}"
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

#Создаем группу безопасности для предоставления доступа к серверу
resource "yandex_vpc_security_group" "group2" {
  name = "Security Group for Nginx with load balancer"
  description = "Security group for accessing traffic to our Nginx Servers for Ansible with load balancer"

  network_id  = "${yandex_vpc_network.network-2.id}"

  ingress {
    port = 80
    protocol = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    port = 22
    protocol = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    port = 81
    protocol = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

}

#Создаем сеть network2
resource "yandex_vpc_network" "network-2" {
  name = "network2"
}

#Создаем подсеть для ВМ
resource "yandex_vpc_subnet" "subnet-2" {
  name = "subnet2"
  zone = "ru-central1-a"
  network_id = yandex_vpc_network.network-2.id
  v4_cidr_blocks = ["192.168.11.0/24"]
}

#Создаем балансировщик наргузки
resource "yandex_lb_network_load_balancer" "nlb-1" {
  name = "react-nlb"
  listener {
    name = "listener1"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.ig-1.load_balancer[0].target_group_id}"
    healthcheck {
      name = "react-checker"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

output "balancer_ip" {
  value = [yandex_lb_network_load_balancer.nlb-1.listener]
}

output "nginx_instances" {
  value = [yandex_compute_instance_group.ig-1.instances]
}

output "react_vm" {
  value = [yandex_compute_instance.vm-1]
}
